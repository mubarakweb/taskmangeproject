<nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">TaskMange</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Dashborad</a></li>
        <li><a href="{{url('/categories')}}">Category</a></li>
        <li><a href="{{ url('/tasks')}}">Task</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <form action="{{ route('logout')}}" method="POST">
       @csrf
      <button type="submit" class="btn btn-link" style="padding-top:15px; text-decoration:none"><span class="glyphicon glyphicon-log-in"></span> Logout</button>

      </form>
    </ul>
    </div>
  </nav>
