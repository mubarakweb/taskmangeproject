@extends('layouts.app')

@section('contents')
<h3>Add New Taks</h3>
<hr>
<form class="form-horizontal" action="{{ url('/tasks') }}" method="POST">
    @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
    @csrf
    <div class="form-group">
        <label class="control-label col-sm-2">Task Name:</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control" value="{{ old('name')}}" placeholder="Enter Task Name"  style="width:500px">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Caetegory:</label>
        <div class="col-sm-10">
            <select name="category_id" class="form-control"  style="width:500px">
                <option value="">--Select a Category--</option>
                @foreach ($categories_list as $item)
                <option value="{{ $item->id }}" {{ old('category_id')==$item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Task details:</label>
        <div class="col-sm-10">
            <textarea name="details" cols="30" rows="10" class="form-control" style="width:500px">{{ old("details") }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Task Deadline:</label>
        <div class="col-sm-10">
           <input type="date" name="deadline" value="{{ old('deadline'); }}" class="form-control"  style="width:500px">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Status:</label>
        <div class="col-sm-10">
           <select name="status" class="form-control"  style="width:500px">
               <option value="">--Select a Status--</option>
               @foreach ($task_status as $key => $status)
                   <option value="{{ $key }}" {{ old('status')==$key ? 'selected' : '' }}>{{ $status }}</option>
               @endforeach
           </select>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Create</button>
        </div>
    </div>
</form>

@endsection
