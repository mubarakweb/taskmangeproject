@extends('layouts.app')

@section('contents')
    <a href="{{ url('/tasks/create') }}" class="btn btn-primary">Add New Task</a>
    <br>
    <br>
    <br>
    <table class="table table table-bordered table-striped ">
        <tr>
            <th scope="col">Task Name</th>
            <th scope="col">Category Name</th>
            <th scope="col">Detalis</th>
            <th scope="col">Deadline</th>
            <th scope="col">Status</th>
            <th>Action</th>
        </tr>
        @foreach ($tasks as $item)
          <tr>
              <td>{{$item->name}}</td>
              <td>{{ $item->name }}</td>
              <td>{{$item->details}}</td>
              <td>{{$item->deadline}}</td>
              <td>{{ App\Enums\TaskStatus::getDescription($item->status) }}</td>
              <td>
                <a href="{{ url("/tasks/$item->id/edit") }}" class="btn btn-success">Update</a>

                <form action="{{ url("/tasks/$item->id") }}" method="POST" onsubmit="return confirm('Do you really want to delete this task?');">
                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>


          </tr>
        @endforeach

    </table>
@endsection
