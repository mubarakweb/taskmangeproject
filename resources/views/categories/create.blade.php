@extends('layouts.app')
@section('contents')
<h3 class="text-danger">Add Category</h3>
<hr>
 <form action="{{ url('/categories')}}" method="POST">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    @csrf
     <div class="form-group ">
         <label class="control-label col-sm-2">Category Name:</label>
        <div class="col-sm-10">
            <input type="text" name="category_name" class="form-control"   style="width:500px">
        </div>
     </div>
      <br>
      <br>


     <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-danger-sm">Add Category</button>
        </div>
    </div>
 </form>
@endsection
