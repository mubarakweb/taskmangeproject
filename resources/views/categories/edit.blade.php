@extends('layouts.app')
@section('contents')
<h3 class="text-danger">Update Category</h3>
<hr>
 <form action="{{ url("/categories/$categorie->id")}}" method="POST">
   @method("put");
    @csrf
     <div class="form-group ">
         <label class="control-label col-sm-2">Category Name:</label>
        <div class="col-sm-10">
            <input type="text" name="category_name" class="form-control" value="{{$categorie->name}}"  style="width:500px"
            >
        </div>
     </div>
      <br>
      <br>


     <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Update Category</button>
        </div>
    </div>

@endsection
