@extends('layouts.app')

@section('contents')
    <a href="{{ url('/categories/create') }}" class="btn btn-primary">Add New Category</a>
    <br>
    <br>
    <br>
    <table class="table table table-bordered table-striped ">
        <tr>
            <th scope="col">Category Name</th>
            <th>Action</th>
        </tr>
        @foreach ($category_list as $cat)
          <tr>
              <td>{{$cat->name}}</td>
              <td>
                <a href="{{ url("/categories/$cat->id/edit") }}" class="btn btn-success">Update</a>
                    <form action="{{ url("/categories/$cat->id") }}"   onsubmit="return confirm('Do you really want to delete this Category?');"method="POST">
                        @csrf
                        @method('delete')

                  <input  type="submit"  class="btn btn-danger btn" value="Delete">
                </form>
              </td>


          </tr>
        @endforeach

    </table>
@endsection
