<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class TaskStatus extends Enum
{
    const Pending  =   0;
    const Ongoing =   1;
    const  Done = 2;
    const  Review = 3;

    public static function getDescription($value): string
{
    if ($value === self::Pending) {
        return 'Super admin';
    }elseif($value==self::Ongoing){
        return 'Exam Controller';
    }elseif($value==self::Done){
        return 'Account Officer';
    }elseif($value==self::Review){
      return 'Course Advisore';
    }
    return parent::getDescription($value);
}
}
