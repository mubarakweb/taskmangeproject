<?php

namespace App\Http\Requests;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use PhpParser\Node\Stmt\Catch_;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_name'=> 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'category_name.required' => 'A Category Name is required',
        ];
    }
}
