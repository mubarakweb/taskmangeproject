<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required",
            'details' => "required",
            'category_id' => "required",
            'status' => "required",
            'deadline' => "date|nullable",
        ];
    }

    public function messages()
    {
        return [
              'name.required' => 'A Task Name is required',
              'details.required'=> "Deletlis Filed is required",
              'category_id.required'=> "Category Filed is required",



        ];
    }

}
